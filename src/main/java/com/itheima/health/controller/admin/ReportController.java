package com.itheima.health.controller.admin;

import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.service.ReportService;
import com.itheima.health.vo.BusinessVO;
import com.itheima.health.vo.MemberVO;
import com.itheima.health.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.Month;

/**
 * PROJECT_NAME : heima-health
 * NAME : MemberController
 * USER : sun
 * DATE : 2023/6/3
 * DESCRIPTION : 统计分析
 */
@Slf4j
@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private ReportService reportService;

    /**
     * 会员数量折线图
     *
     * @return 全局通用返回值bean(封装成前台需要的vo对象)
     */
    @GetMapping("/getMemberReport")
    public Result getMemberReport() {
        LocalDate now = LocalDate.now();
        log.info("[会员数量]");
        MemberVO memberVo = reportService.getMemberReport(now);
        return new Result(true, MessageConst.GET_MEMBER_NUMBER_REPORT_SUCCESS, memberVo);
    }

    @GetMapping("/getSetmealReport")
    public Result getSetmealReport() {
        log.info("[套餐占比]");
        SetmealVO setmealVo = reportService.getSetmealReport();
        return new Result(true, MessageConst.GET_SETMEAL_COUNT_REPORT_SUCCESS, setmealVo);
    }

    @GetMapping("/getBusinessReportData")
    public Result getBusinessReportData() {
        log.info("[运营统计]");
        BusinessVO businessVO = reportService.getBusinessReportData();
        return new Result(true, MessageConst.GET_BUSINESS_REPORT_SUCCESS, businessVO);
    }

    /**
     * 报表导出
     *
     * @return
     */
    @GetMapping("/exportBusinessReport")
    public Result exportBusinessReport(HttpServletResponse response) {
        log.info("[运营统计报表导出]");
        reportService.export(response);
        return new Result(true, MessageConst.GET_BUSINESS_REPORT_SUCCESS );
    }


}
