package com.itheima.health.controller.admin;

import com.itheima.health.common.MessageConst;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.User;
import com.itheima.health.service.UserService;
import com.itheima.health.vo.LoginParam;
import lombok.extern.slf4j.Slf4j;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author zhangmeng
 * @description
 * @date 2019/9/6
 **/
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    private static final String CURRENT_USER = "CURRENT_USER";
    @Autowired
    private UserService userService;

    /**
     * 根据用户名和密码登录
     *
     * @param request
     * @return
     */
    @PostMapping("/login")
    public Result login(HttpServletRequest request, @RequestBody LoginParam param) {
        log.info("[登录]data:{}", param);
        //rpc调用查询用户信息
        User user = userService.findByUsername(param.getUsername());
        if (user == null) {
            log.info("[登录]失败，user:{}", param.getUsername());
            return new Result(false, "没有该用户");
        }
        //用户不存在或密码不匹配则登录失败
        String password = DigestUtils.md5DigestAsHex(param.getPassword().getBytes());
        //判断是否为导入的密码，加密为{md5}
        if (user.getPassword().startsWith("{md5}")) {
            password = "{md5}" + password;
        }
        //判断是否为导入的密码，加密为{bcrypt}
        if (user.getPassword().startsWith("{bcrypt}")) {
            password = "{bcrypt}" + BCrypt.hashpw(param.getPassword(), "$2a$10$u/BcsUUqZNWUxdmDhbnoeeobJy6IBsL1Gn");
        }
        if (null == user || !user.getPassword().equals(password)) {
            log.info("[登录]失败，user:{}", param.getUsername());

            return new Result(false, MessageConst.LOGIN_FAIL);
        }
        //模拟用户登录成功
        request.getSession().setAttribute("user", user.getUsername());
        String role=userService.findRoleByUserid(user.getId());
        log.info("[登录]成功，user:{},并给用户下发身份标识{}", user.getUsername(), user.getUsername());
        return new Result(true, role);
    }

    @GetMapping("/logout")
    public Result logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return new Result(true, MessageConst.ACTION_SUCCESS);
    }

    /**
     * 没有权限访问的路径
     *
     * @return
     */
    @RequestMapping("/role/error")
    public Result error(HttpServletRequest request, HttpServletResponse response) {

        return new Result(false, "权限被限制");
    }
}
