package com.itheima.health.controller.user;

import com.itheima.health.common.MessageConst;
import com.itheima.health.dto.SMSDTO;
import com.itheima.health.dto.SubmitOrderDTO;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.AliSMSService;
import com.itheima.health.service.SetMealService;
import com.itheima.health.service.VerifySMSService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhangmeng
 * @description 套餐控制器
 * @date 2019/9/26
 **/
@RestController
@RequestMapping("/mobile")
@Slf4j
public class MobileSetMealController {
    @Autowired
    private SetMealService setMealService;
    @Autowired
    private AliSMSService aliSMSService;
    @Autowired
    private VerifySMSService verifySMSService;
    /**
     * C端-套餐查询
     * @return
     */
    @GetMapping("/setmeal/getSetmeal")
    public Result getSetmeal(){
        log.info("C端-套餐查询");
        List<Setmeal> setmeal = setMealService.getSetmeal();
        return new Result(true,MessageConst.QUERY_SETMEAL_SUCCESS,setmeal);
    }

    /**
     * C端-套餐详情查询
     * @param id 接收到的套餐Id
     * @return
     */
    @GetMapping("/setmeal/findById")
    public Result findById(Integer id){
        log.info("C端-套餐详情查询");
        Setmeal setmeal = setMealService.findById(id);
        return new Result(true,MessageConst.QUERY_SETMEAL_SUCCESS,setmeal);
    }

    /**
     * *
     * @param type-传入缓存的类型
     * @param telephone-需要验证的手机号
     * @return
     * @throws Exception
     */
    @PostMapping("/validateCode/send")
    public Result loginSendSMS( String type , String telephone) throws Exception {
        log.info("[开始发送手机验证码]");
        aliSMSService.loginSendSMS(type,telephone);
        return new Result(true,MessageConst.SEND_VALIDATECODE_SUCCESS);
    }
    @PostMapping("/login/smsLogin")
    public Result smsLogin(@RequestBody SMSDTO smsdto){
        verifySMSService.smsLogin(smsdto);
        return new Result(true,MessageConst.LOGIN_SUCCESS);
    }
}
