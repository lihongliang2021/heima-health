package com.itheima.health.controller.user;


import com.itheima.health.common.MessageConst;
import com.itheima.health.dto.SubmitOrderDTO;
import com.itheima.health.entity.Result;
import com.itheima.health.pojo.Order;
import com.itheima.health.service.OrderService;
import com.itheima.health.vo.SubmitVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mobile/order")
@Slf4j
public class MobileOrderController {

    //注入订单预约服务层
    @Resource
    private OrderService orderService;

    /**
     * 用户提交预约
     * @param submitOrderDTO 用于接收保存了用户预约的信息类
     * @return 全局通用返回信息
     */
    @PostMapping("/submit")
    public Result submit(@RequestBody SubmitOrderDTO submitOrderDTO){
        log.info("[开始提交预约]接收到的预约信息为:{}",submitOrderDTO);
        Order order = orderService.submit(submitOrderDTO);
        return new Result(true, MessageConst.ORDER_SUCCESS,order);
    }

    /**
     * 通过id查询预约成功信息
     * @param id 订单id
     * @return 全局通用返回信息:包含了保存了预约成功信息的VO对象
     */
    @GetMapping("/findById")
    public Result findSubmintMseeageById(Integer id){
        log.info("[预约成功后通过id进行数据回显]Date:id:{}",id);
        SubmitVO submitMessage = orderService.findSubmintMseeageById(id);
        return new Result(true,MessageConst.QUERY_ORDER_SUCCESS,submitMessage);
    }
}
