package com.itheima.health.service;

import com.itheima.health.dto.SubmitOrderDTO;
import com.itheima.health.pojo.Order;
import com.itheima.health.vo.SubmitVO;

public interface OrderService {


    /**
     * 用户提交预约
     * @param submitOrderDTO 用于接收保存了用户预约的信息类
     * @return 全局通用返回信息
     */

    Order submit(SubmitOrderDTO submitOrderDTO);

    /**
     * 通过id查询预约成功信息
     * @param id 订单id
     * @return 保存了预约成功信息的VO对象
     */
    SubmitVO findSubmintMseeageById(Integer id);
}
