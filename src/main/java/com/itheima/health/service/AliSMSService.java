package com.itheima.health.service;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: AliSMSService
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
public interface AliSMSService {
    void loginSendSMS(String type, String telephone) throws Exception;
}
