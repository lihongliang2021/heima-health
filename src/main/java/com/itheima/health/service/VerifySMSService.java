package com.itheima.health.service;

import com.itheima.health.dto.SMSDTO;
import com.itheima.health.dto.SubmitOrderDTO;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: VerifySMSService
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
public interface VerifySMSService {
    void smsLogin(SMSDTO smsdto);
}
