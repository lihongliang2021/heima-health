package com.itheima.health.service;

import com.itheima.health.vo.BusinessVO;
import com.itheima.health.vo.MemberVO;
import com.itheima.health.vo.SetmealVO;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

/**
 * PROJECT_NAME : heima-health
 * NAME : ReportService
 * USER : sun
 * DATE : 2023/6/3
 * DESCRIPTION : 统计分析
 */
public interface ReportService {
    MemberVO getMemberReport(LocalDate now);

    SetmealVO getSetmealReport();

    BusinessVO getBusinessReportData();

    /**
     * 报表导出
     *
     * @return
     */
    void export(HttpServletResponse response);
}
