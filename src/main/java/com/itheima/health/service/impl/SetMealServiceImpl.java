package com.itheima.health.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.health.dao.CheckGroupDao;
import com.itheima.health.dao.CheckItemDao;
import com.itheima.health.dao.SetMealDao;
import com.itheima.health.entity.PageResult;
import com.itheima.health.entity.QueryPageBean;
import com.itheima.health.pojo.CheckGroup;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Setmeal;
import com.itheima.health.service.SetMealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author zhangmeng
 * @description 套餐SEVICE实现类
 * @date 2019/9/26
 **/
@Service
@Slf4j
public class SetMealServiceImpl implements SetMealService {
    @Autowired
    private SetMealDao setMealDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private CheckGroupDao checkGroupDao;
    @Autowired
    private CheckItemDao checkItemDao;
    @Override
    public void add(Setmeal setmeal, Integer[] checkgroupIds) {
        log.info("[套餐-添加]data:{},checkgroupIds:{}",setmeal,checkgroupIds);
        // 调用DAO数据入库
        // 插入基本数据
        setMealDao.insert(setmeal);
        // 插入关联数据
        for (Integer checkgroupId : checkgroupIds) {
            setMealDao.insertSetMealAndCheckGroup(setmeal.getId(),checkgroupId);
        }
        redisTemplate.opsForSet().add("ADD_URL",setmeal.getImg());
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //设置分页参数
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        //DAO查询数据
        Page<Setmeal> page = setMealDao.selectByCondition(queryPageBean.getQueryString());
        //封装返回值
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public List<Setmeal> getSetmeal() {
        return setMealDao.selectSetmeal();
    }

    @Override
    public Setmeal findById(Integer id) {
        Setmeal setmeal = setMealDao.selectSetmealById(id);
        //创建用于接收套餐内包含的检查组数组对象
        ArrayList<CheckGroup> checkGroups = new ArrayList<>();
        //根据套餐id查询该套餐包含的检查组id
        List<Integer> checkGroupIds = checkGroupDao.selectSetmealCheckGroupById(id);
        //遍历套餐中包含的检查组id
        for (Integer checkGroupId : checkGroupIds) {
            //根据checkGroupId查询每条检查组详细信息，添加到checkGroups集合中保存
            CheckGroup checkGroup = checkGroupDao.selectCheckGroupBySetmealId(checkGroupId);
            checkGroups.add(checkGroup);
        }
        //遍历保存检查组信息的集合
        for (CheckGroup checkGroup : checkGroups) {
            //查询每条检查组中包含的检查项id
            List<Integer> checkitem = checkItemDao.selectCheckGroupCheckitemById(checkGroup.getId());
            //创建用于接收检查组中包含的检查项集合对象
            ArrayList<CheckItem> checkItems = new ArrayList<>();
            //查询每个检查组中包含的检查项，存储到checkItems集合中
            for (Integer checkitemId : checkitem) {
                CheckItem checkItem = checkItemDao.selectById(checkitemId);
                checkItems.add(checkItem);
            }
            //将保存了检查项详细信息的集合存储到检查组中
            checkGroup.setCheckItems(checkItems);
        }
        //将保存了检查组的集合详细信息存储到套餐对象中
        setmeal.setCheckGroups(checkGroups);
//        CheckGroup checkGroup = checkGroupDao.selectCheckGroupBySetmealId(setmeal.getId());
        /*List<Integer> checkGroupIds = checkGroupDao.selectSetmealCheckGroupById(id);
        checkGroupIds.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer id) {
                ArrayList<CheckItem> checkItems = new ArrayList<>();
                CheckGroup checkGroup = checkGroupDao.selectCheckGroupById(id);
                List<Integer> checkItemIds = checkItemDao.selectCheckGroupCheckitemById(id);
                checkItemIds.forEach(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer id) {
                        checkItems.add(checkItemDao.selectCheckGroupById(id));
                    }
                });
                checkGroup.setCheckItems(checkItems);
            }
        });
        setmeal.setCheckGroups(checkGroups);*/
        return setmeal;
    }
}
