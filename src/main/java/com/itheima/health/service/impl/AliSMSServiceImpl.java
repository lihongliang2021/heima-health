package com.itheima.health.service.impl;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.tea.TeaException;
import com.aliyun.teautil.models.RuntimeOptions;
import com.itheima.health.properties.AliSMSProperties;
import com.itheima.health.service.AliSMSService;
import com.itheima.health.util.AlismsUtils;
import com.itheima.health.util.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: AliSMSServiceImpl
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
@Slf4j
@Service
public class AliSMSServiceImpl implements AliSMSService {
    @Resource
    private AliSMSProperties aliSMSProperties;
    @Resource
    private AlismsUtils alismsUtils;
    @Resource
    private RedisTemplate redisTemplate;
    @Transactional
    @Override
    public synchronized void loginSendSMS(String type, String telephone) throws Exception {
        Integer code = ValidateCodeUtils.generateValidateCode(4);
        log.info("[验证码]:{}",code);
        Client client = alismsUtils.createClient();
       SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName(aliSMSProperties.getSignName())
                .setTemplateCode(aliSMSProperties.getTemplateCode())
                .setPhoneNumbers(telephone)
                .setTemplateParam("{code:"+code+"}");
        RuntimeOptions runtime = new RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            client.sendSmsWithOptions(sendSmsRequest, runtime);
        } catch (TeaException error) {
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
        if (type.equals("MOBILE_SIGNIN")){
            redisTemplate.opsForValue().set("MOBILE_SIGNIN"+telephone,code,180, TimeUnit.SECONDS);
        }else {
            redisTemplate.opsForValue().set("ORDER"+telephone,code,180, TimeUnit.SECONDS);
        }
    }
}
