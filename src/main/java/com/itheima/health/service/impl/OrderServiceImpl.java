package com.itheima.health.service.impl;

import com.itheima.health.common.MessageConst;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.dao.OrderDao;

import com.itheima.health.dto.SubmitOrderDTO;
import com.itheima.health.exception.BusinessRuntimeException;
import com.itheima.health.pojo.Member;
import com.itheima.health.pojo.Order;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.service.OrderService;
import com.itheima.health.vo.SubmitVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    //注入订单持久层
    @Resource
    private OrderDao orderDao;
    @Resource
    private MemberDao memberDao;
    @Resource
    private RedisTemplate redisTemplate;


    /**
     * 用户提交预约
     *
     * @param submitOrderDTO 用于接收保存了用户预约的信息类
     * @return 全局通用返回信息
     */
    @Transactional
    @Override
    public Order submit(SubmitOrderDTO submitOrderDTO) {
         /*1.判断手机验证码是否正确
    当验证码正确，日期必须要填写，后台要根据日期进行预约操作，要求提交表单之前 日期必须填写
    如果错误:提示VALIDATECODE_ERROR = "验证码输入错误";*/
        log.info("获取到的验证码:{}",submitOrderDTO.getValidateCode());
        Boolean hasKey = redisTemplate.hasKey("ORDER" + submitOrderDTO.getTelephone());
        if (Boolean.FALSE.equals(hasKey)){
            throw new BusinessRuntimeException(MessageConst.VALIDATECODE_ERROR);
        }
        /*2.判断是否已经注册会员:通过身份证id进行查询数量
     如果count=0不存在 , 即未注册, 需要自注册成为会员-->添加会员表,如果count>0存在*/
        Member uploadMember = memberDao.selectBytelephone(submitOrderDTO.getTelephone());

        if(Objects.nonNull(uploadMember)){ //不为空
            //实际情况因为再添加手机号的时候一定是注册了的,所以这里做更新操作
            uploadMember.setName(submitOrderDTO.getName());
            uploadMember.setSex(submitOrderDTO.getSex());
            uploadMember.setIdCard(submitOrderDTO.getIdCard());
            uploadMember.setBirthday(submitOrderDTO.getbirthDayByIdCard(uploadMember.getIdCard()));//通过身份证信息获取生日日期
            uploadMember.setPassword("123456");//设置默认密码
            log.info("[本次更新的uploadMember] 信息是:{}",uploadMember);
            memberDao.uploadMember(uploadMember);//更新会员信息
        }else{
            //如果通过手机查找Member对象为空的话,进行自动注册
            SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(System.currentTimeMillis());
            Member newMember = new Member();
            BeanUtils.copyProperties(submitOrderDTO,newMember);//赋值了姓名,性别,身份证号,手机号,
            newMember.setRegTime(date);//设置注册日期
            newMember.setBirthday(submitOrderDTO.getbirthDayByIdCard(newMember.getIdCard()));//通过身份证信息获取生日日期
            newMember.setPhoneNumber(submitOrderDTO.getTelephone());//设置手机号
            newMember.setPassword("123456");//设置默认密码
            memberDao.insert(newMember);//自动注册为会员

        }

    /*3.根据会员member表中的id和订单表中的id对比,比较对应日期orderDate是否有预约相同套餐setmeal_id,
        如果有:提示HAS_ORDERED = "已经完成预约，不能重复预约,
        如果没有,通过ordersetting表中对应日期的预约人数,判断已预约人数reservations是否小于可预约人数number,小于则预约成功,=>则预约失败提示:ORDER_FULL = "预约已满"; */
        Integer num = 0;//用来计数是否有重复预约订单
        Member member = memberDao.selectBytelephone(submitOrderDTO.getTelephone());
        num = orderDao.selectOrderNum(member.getId(),submitOrderDTO.getSetmealId(), submitOrderDTO.getOrderDate());//会员id,套餐id,预约日期
        OrderSetting orderSetting =  orderDao.selectordSettingByOrderDate(submitOrderDTO.getOrderDate());//通过预约日期查询预约人数设置信息
        Order order = new Order();
        if(num > 0){
            throw new BusinessRuntimeException(MessageConst.HAS_ORDERED);//重复预约
        }else if(orderSetting.getReservations() >= orderSetting.getNumber()){
            throw new BusinessRuntimeException(MessageConst.ORDER_FULL);//预约已满
        }else{
            //添加预约信息到order表中
            BeanUtils.copyProperties(submitOrderDTO,order);//赋值预约日期,预约的套餐id
            order.setMemberId(member.getId());
            order.setOrderType(Order.ORDERTYPE_WEIXIN);
            order.setOrderStatus(Order.ORDERSTATUS_NO);
            orderDao.insertOrder(order);
            orderSetting.setReservations(orderSetting.getReservations()+1);//预约人数+1

            orderDao.updateOrdSetting(orderSetting);//更新预约设置
        }
        return order;
    }


    /**
     * 通过id查询预约成功信息
     *
     * @param id 订单id
     * @return 保存了预约成功信息的VO对象
     */
    @Override
    public SubmitVO findSubmintMseeageById(Integer id) {
        return orderDao.findSubmintMseeageById(id);
    }
}

