package com.itheima.health.service.impl;

import com.itheima.health.dao.ReportDao;
import com.itheima.health.entity.HotSetmeal;
import com.itheima.health.entity.SetmealCount;
import com.itheima.health.service.ReportService;
import com.itheima.health.vo.BusinessVO;
import com.itheima.health.vo.MemberVO;
import com.itheima.health.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.function.Consumer;

/**
 * PROJECT_NAME : heima-health
 * NAME : ReportServiceImpl
 * USER : sun
 * DATE : 2023/6/3
 * DESCRIPTION : 统计分析
 */
@Service
@Slf4j
public class ReportServiceImpl implements ReportService {
    @Autowired
    private ReportDao reportDao;

    @Override
    public MemberVO getMemberReport(LocalDate now) {
        ArrayList<LocalDate> months = new ArrayList<>();
        ArrayList<Object> list = new ArrayList<>();
        ArrayList<Integer> memberCount = new ArrayList<>();
        LocalDate beginDate = LocalDate.of(now.getYear()-1, now.getMonth().plus(1), 1);
       // LocalDate beginDate = LocalDate.of(2019, 9, 1);
        LocalDate of = LocalDate.of(1970, 1, 1);
        months.add(beginDate);
        int i = 0;
        //将近12个月封装到List集合中
        while (months.size() < 12) {
            months.add(months.get(i).plusMonths(1));
            i++;
        }
        //将每个月份的数据个数查询封装到List集合中
        i = 0;

        for (LocalDate localDate : months) {
            LocalDate end = LocalDate.of(localDate.getYear(), localDate.getMonth().plus(1), 1);
            Integer count = reportDao.selectMemberCountByDate(of, end);
            i+=count;
            memberCount.add(count+ i);
        }

        return MemberVO.builder().months(months).memberCount(memberCount).build();
    }

    @Override
    public SetmealVO getSetmealReport() {
        ArrayList<String> setmealNamesList = new ArrayList<>();
        List<SetmealCount> setmealCountList = reportDao.selectSetmealAndOrder();
        setmealCountList.forEach(new Consumer<SetmealCount>() {
            @Override
            public void accept(SetmealCount setmealCount) {
                setmealNamesList.add(setmealCount.getName());
            }
        });
        return SetmealVO.builder().setmealNames(setmealNamesList).setmealCount(setmealCountList).build();
    }

    @Override
    public BusinessVO getBusinessReportData() {
        BusinessVO businessVO = new BusinessVO();
        businessVO.setReportDate(LocalDate.now().toString());
        Integer todayNewMember = reportDao.selectMemberCountByDate(LocalDate.now(), LocalDate.now());
        Integer thisMonthNewMember = reportDao.selectMemberCountByDate(LocalDate.now().minusMonths(1), LocalDate.now());
        Integer thisWeekNewMember = reportDao.selectMemberCountByDate(LocalDate.now().minusWeeks(1), LocalDate.now());
        Integer totalMember = reportDao.selectMemberCountAll();
        businessVO.setTodayNewMember(todayNewMember);
        businessVO.setThisMonthNewMember(thisMonthNewMember);
        businessVO.setThisWeekNewMember(thisWeekNewMember);
        businessVO.setTotalMember(totalMember);


        Integer memberCountAll = reportDao.selectMemberCountAll();
        List<SetmealCount> setmealCountList = reportDao.selectSetmealAndOrder();
        ArrayList<HotSetmeal> hotSetmeals = new ArrayList<>();
        setmealCountList.forEach(new Consumer<SetmealCount>() {
            @Override
            public void accept(SetmealCount setmealCount) {
                HotSetmeal hotSetmeal = new HotSetmeal();
                BeanUtils.copyProperties(setmealCount, hotSetmeal);
                hotSetmeal.setSetmeal_count(setmealCount.getValue());
                Integer value = setmealCount.getValue();
                Integer memberCountAll1 = memberCountAll;
//                double v = value.doubleValue() / memberCountAll1.doubleValue() / 1.0;
//                Number number = new Double();
                BigDecimal bg = new BigDecimal(value.doubleValue() / memberCountAll1.doubleValue() / 1.0);
                double f1 = bg.setScale(4, BigDecimal.ROUND_HALF_UP).doubleValue();
                hotSetmeal.setProportion(f1);
                hotSetmeals.add(hotSetmeal);
            }
        });
        businessVO.setHotSetmeal(hotSetmeals);

        Integer todayVisitsNumber = reportDao.selectOrderCount("已到诊", LocalDate.now(), LocalDate.now());
        Integer thisWeekVisitsNumber = reportDao.selectOrderCount("已到诊", LocalDate.now().minusWeeks(1), LocalDate.now());
        Integer thisMonthVisitsNumber = reportDao.selectOrderCount("已到诊", LocalDate.now().minusMonths(1), LocalDate.now());
        businessVO.setTodayVisitsNumber(todayVisitsNumber);
        businessVO.setThisWeekVisitsNumber(thisWeekVisitsNumber);
        businessVO.setThisMonthVisitsNumber(thisMonthVisitsNumber);

        Integer todayOrderNumber = reportDao.selectOrderCount("", LocalDate.now(), LocalDate.now());
        Integer thisWeekOrderNumber = reportDao.selectOrderCount("", LocalDate.now().minusWeeks(1), LocalDate.now());
        Integer thisMonthOrderNumber = reportDao.selectOrderCount("", LocalDate.now().minusMonths(1), LocalDate.now());
        businessVO.setTodayOrderNumber(todayOrderNumber);
        businessVO.setThisWeekOrderNumber(thisWeekOrderNumber);
        businessVO.setThisMonthOrderNumber(thisMonthOrderNumber);
        return businessVO;
    }

    /**
     * 报表导出
     *
     * @return
     */
    @Override
    public void export(HttpServletResponse response) {
        //获取报表数据，提供给Excel模板对象
        BusinessVO businessVO = getBusinessReportData();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("template/report_template.xlsx");//如果没有读到创建Excel表格时会报错
        try {
            //基于提供好的模板文件创建一个新的Excel表格对象
            XSSFWorkbook excel = new XSSFWorkbook(inputStream);
            //获取Excel文件中的Sheet1页
            XSSFSheet sheet1 = excel.getSheet("Sheet1");
            //在格子里添加日期
            sheet1.getRow(2).getCell(5).setCellValue(businessVO.getReportDate());
            //添加一列
            sheet1.getRow(4).getCell(5).setCellValue(businessVO.getTodayNewMember());
            sheet1.getRow(5).getCell(5).setCellValue(businessVO.getThisWeekNewMember());
            sheet1.getRow(7).getCell(5).setCellValue(businessVO.getTodayOrderNumber());
            sheet1.getRow(8).getCell(5).setCellValue(businessVO.getThisWeekOrderNumber());
            sheet1.getRow(9).getCell(5).setCellValue(businessVO.getThisMonthOrderNumber());
            //添加一列
            sheet1.getRow(4).getCell(7).setCellValue(businessVO.getTotalMember());
            sheet1.getRow(5).getCell(7).setCellValue(businessVO.getThisMonthNewMember());
            sheet1.getRow(7).getCell(7).setCellValue(businessVO.getThisWeekVisitsNumber());
            sheet1.getRow(8).getCell(7).setCellValue(businessVO.getThisWeekVisitsNumber());
            sheet1.getRow(9).getCell(7).setCellValue(businessVO.getThisMonthVisitsNumber());
            //添加热门套餐
            for (int i = 0; i < businessVO.getHotSetmeal().size(); i++) {
                XSSFRow row = sheet1.getRow(12 + i);
                HotSetmeal hotSetmeal = businessVO.getHotSetmeal().get(i);
                row.getCell(4).setCellValue(hotSetmeal.getName());
                row.getCell(5).setCellValue(hotSetmeal.getSetmeal_count());
                row.getCell(6).setCellValue(hotSetmeal.getProportion().toString());
            }
            // 设置文件名的
            response.setHeader("Content-Disposition", "attachment;filename*=UTF-8''" + URLEncoder.encode("运营数据统计表.xlsx", "UTF-8").replace("+", "%20"));
            ServletOutputStream outputStream = response.getOutputStream();
            excel.write(outputStream);
            //关闭资源
            outputStream.flush();
            outputStream.close();
            excel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
