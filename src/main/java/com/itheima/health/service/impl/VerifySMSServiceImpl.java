package com.itheima.health.service.impl;

import com.itheima.health.common.MessageConst;
import com.itheima.health.dao.MemberDao;
import com.itheima.health.dto.SMSDTO;
import com.itheima.health.dto.SubmitOrderDTO;
import com.itheima.health.exception.BusinessRuntimeException;
import com.itheima.health.pojo.CheckItem;
import com.itheima.health.pojo.Member;
import com.itheima.health.service.VerifySMSService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: VerifySMSServiceImpl
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
@Slf4j
@Service
public class VerifySMSServiceImpl implements VerifySMSService {


    @Resource
    private MemberDao memberDao;
    @Resource
    private RedisTemplate redisTemplate;
    @Override

    public synchronized void smsLogin(SMSDTO smsdto) {
        log.info("获取到的验证码:{}",smsdto.getValidateCode());
        Boolean hasKey = redisTemplate.hasKey("MOBILE_SIGNIN" + smsdto.getTelephone());
        if (Boolean.FALSE.equals(hasKey)){
            throw new BusinessRuntimeException(MessageConst.VALIDATECODE_ERROR);
        }else {
            Member member=memberDao.selectBytelephone(smsdto.getTelephone());
            if(Objects.isNull(member)){
                Member member1=new Member();
                member1.setPhoneNumber(smsdto.getTelephone());
                SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date(System.currentTimeMillis());
                member1.setRegTime(date);
                memberDao.insert(member1);
            }
        }
    }
}
