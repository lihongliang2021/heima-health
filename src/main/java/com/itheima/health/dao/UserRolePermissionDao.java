package com.itheima.health.dao;

import com.itheima.health.pojo.Permission;
import com.itheima.health.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Set;

/**
 * PROJECT: heima-health
 * PROJECT_NAME: com.itheima.health.dao
 * CLASSNAME: UserRolePermissionDao
 * DATE: 06/2023/6/4
 * DESCRIPTION : 用户角色权限关系
 */
@Mapper
public interface UserRolePermissionDao {
    @Select("SELECT keyword FROM itcast_health.t_permission WHERE  id in (SELECT permission_id FROM itcast_health.t_role_permission WHERE role_id = (SELECT role_id FROM itcast_health.t_user_role WHERE user_id = (SELECT id FROM itcast_health.t_user WHERE username = #{user})));")
    List<String> selectPermission(@Param("user") String user);
}
