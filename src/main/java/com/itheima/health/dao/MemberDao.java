package com.itheima.health.dao;

import com.itheima.health.dto.SubmitOrderDTO;
import com.itheima.health.pojo.Member;
import org.apache.ibatis.annotations.Param;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: Member
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
public interface MemberDao {

    Member selectBytelephone(String telephone);

    void insert(Member member);


    /**
     * 更新会员信息
     * @param member 保存了更新信息的会员对象
     */
    void uploadMember(@Param("member") Member member);
}
