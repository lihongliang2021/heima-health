package com.itheima.health.dao;

import com.itheima.health.entity.SetmealCount;
import com.itheima.health.pojo.Order;
import com.itheima.health.vo.MemberVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.List;

/**
 * PROJECT_NAME : heima-health
 * NAME : ReportDao
 * USER : sun
 * DATE : 2023/6/3
 * DESCRIPTION :
 */
@Mapper
public interface ReportDao {

    Integer selectMemberCountByDate(@Param("begin") LocalDate begin,@Param("end") LocalDate end);

    List<SetmealCount> selectSetmealAndOrder();

    Integer selectMemberCountAll();

    Integer selectOrderCount(@Param("orderStatus") String orderStatus, @Param("begin") LocalDate begin, @Param("end") LocalDate end);
}
