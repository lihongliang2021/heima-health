package com.itheima.health.dao;

import com.itheima.health.pojo.Order;
import com.itheima.health.pojo.OrderSetting;
import com.itheima.health.vo.SubmitVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

@Mapper
public interface OrderDao {


    /**
     * 查询重复预约订单的数量
     * @param memberId  会员id
     * @param setmealId 提交预约的套餐id
     * @param orderDate 预约日期
     * @return 重复预约订单的数量
     */
    Integer selectOrderNum(@Param("memberId") Integer memberId, @Param("setmealId") Integer setmealId, @Param("orderDate") Date orderDate);


    /**
     * 通过预约日期查询预约人数设置信息
     * @param orderDate 预约日期
     * @return 预约设置对象
     */
    OrderSetting selectordSettingByOrderDate(@Param("orderDate") Date orderDate);


    /**
     * 添加订单
     * @param order 订单信息
     */
    void insertOrder(@Param("order") Order order);


    /**
     * 预约成功后当天预约人数加1
     * @param orderSetting 更新后的预约设置
     */
    void updateOrdSetting(@Param("orderSetting") OrderSetting orderSetting);

    /**
     * 通过id查询预约成功信息
     * @param id 订单id
     * @return 保存了预约成功信息的VO对象
     */
    SubmitVO findSubmintMseeageById(@Param("orderId") Integer orderId);
}
