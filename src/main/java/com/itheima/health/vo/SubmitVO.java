package com.itheima.health.vo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubmitVO {

    private String orderType;//订单类型
    private String member;//会员姓名
    private Date orderDate;//预约日期
    private String setmeal;//套餐名

}
