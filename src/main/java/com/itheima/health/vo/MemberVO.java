package com.itheima.health.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

/**
 * PROJECT_NAME : heima-health
 * NAME : MemberVO
 * USER : sun
 * DATE : 2023/6/3
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MemberVO {
    private List<LocalDate> months;
    private List<Integer> memberCount;
}
