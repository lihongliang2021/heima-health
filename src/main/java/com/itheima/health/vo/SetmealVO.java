package com.itheima.health.vo;

import com.itheima.health.entity.SetmealCount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * PROJECT_NAME : heima-health
 * NAME : SetmealVo
 * USER : sun
 * DATE : 2023/6/4
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetmealVO {
    private List<String> setmealNames;
    private List<SetmealCount> setmealCount;
}
