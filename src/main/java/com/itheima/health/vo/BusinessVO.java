package com.itheima.health.vo;

import com.itheima.health.entity.HotSetmeal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * PROJECT_NAME : heima-health
 * NAME : BusinessVO
 * USER : sun
 * DATE : 2023/6/4
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BusinessVO {
    private Integer todayVisitsNumber;
    private String reportDate;
    private Integer todayNewMember;
    private Integer thisWeekVisitsNumber;
    private Integer thisWeekOrderNumber;
    private Integer thisMonthNewMember;
    private Integer thisWeekNewMember;
    private Integer totalMember;
    private Integer thisMonthOrderNumber;
    private Integer thisMonthVisitsNumber;
    private Integer todayOrderNumber;
    private List<HotSetmeal> hotSetmeal;
}
