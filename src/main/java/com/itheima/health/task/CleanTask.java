package com.itheima.health.task;

import com.itheima.health.util.QiniuUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Set;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: CleanTask
 * USER: 10341
 * DATE: 2023/6/4
 * DESCRIPTION :
 */
@Slf4j
@Component
public class CleanTask {
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private QiniuUtils qiniuUtils;
    @Scheduled(cron = "0 0/5 * * * ? ")
    public void clean(){
        log.info("[开始启动清除服务器中的垃圾图片]");
        //获取两个redis中的差集 (UPLOAD不包含ADD中的图片名称)
        Set difference = redisTemplate.opsForSet().difference("UPLOAD_URL", "ADD_URL");
        //遍历此集合得到每一个垃圾图片名称,,再用qiniuUtils删除云端图片
        difference.forEach(o -> {
            log.info("[服务器中垃圾图片名称]data:{}",o);
            qiniuUtils.deleteFileFromQiniu((String) o);
        });
        log.info("[删除云端垃圾服务完毕]");
    }
}
