package com.itheima.health.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: AliSMSProperties
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
@Component
@ConfigurationProperties(prefix = "alisms")
@Data
public class AliSMSProperties {
    private String accessKeyId;
    private String accessSecret;
    private String templateCode;
    private String signName;
}
