package com.itheima.health.util;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.itheima.health.properties.AliSMSProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.javassist.tools.reflect.Sample;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: AlismsUtils
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
@Slf4j
@Data
@AllArgsConstructor
@Component
public class AlismsUtils {
    @Resource
    private AliSMSProperties aliSMSProperties;

    public  Client createClient() throws Exception {
        Config config = new Config()
                // 必填，您的 AccessKey ID
                .setAccessKeyId(aliSMSProperties.getAccessKeyId())
                // 必填，您的 AccessKey Secret
                .setAccessKeySecret(aliSMSProperties.getAccessSecret());
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new Client(config);
    }
}
