package com.itheima.health.filter;

import com.itheima.health.dao.UserRolePermissionDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * PROJECT: heima-health
 * PROJECT_NAME: com.itheima.health.interceptor
 * CLASSNAME: LoginCheckFilter
 * DATE: 06/2023/6/4
 * DESCRIPTION : 权限控制过滤器
 */
@Component
@Slf4j
@WebFilter(urlPatterns = "/*") //过滤所有请求
public class LoginCheckFilter implements Filter {

    //权限路径对应集合
    static Map<String, String> pUrl = new HashMap<>();

    static {
        pUrl.put("CHECKITEM_ADD", "/checkitem/add");
        pUrl.put("CHECKITEM_DELETE", "/checkitem/delete");
        pUrl.put("CHECKITEM_EDIT", "/checkitem/edit");
        pUrl.put("CHECKITEM_QUERY", "/checkitem/findPage");
        pUrl.put("CHECKGROUP_ADD", "/checkgroup/add");
        pUrl.put("CHECKGROUP_DELETE", "/checkgroup/deleteCheckGroupitemById");
        pUrl.put("CHECKGROUP_EDIT", "/checkgroup/edit");
        pUrl.put("CHECKGROUP_QUERY", "/checkgroup/findPage");
        pUrl.put("SETMEAL_ADD", "/setmeal/add");
        pUrl.put("SETMEAL_DELETE", "--");
        pUrl.put("SETMEAL_EDIT", "--");
        pUrl.put("SETMEAL_QUERY", "/setmeal/findPage");
        pUrl.put("ORDERSETTING", "/ordersetting/");//*
        pUrl.put("REPORT_VIEW", "/report/");//*
        pUrl.put("MENU_ADD", "--");
        pUrl.put("MENU_DELETE", "--");
        pUrl.put("MENU_EDIT", "--");
        pUrl.put("MENU_QUERY", "--");
        pUrl.put("ROLE_ADD", "--");
        pUrl.put("ROLE_DELETE", "--");
        pUrl.put("ROLE_EDIT", "--");
        pUrl.put("ROLE_QUERY", "--");
        pUrl.put("USER_ADD", "--");
        pUrl.put("USER_DELETE", "--");
        pUrl.put("USER_EDIT", "--");
        pUrl.put("USER_QUERY", "--");
    }

    @Autowired
    UserRolePermissionDao userRolePermissionDao;

    /**
     *
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        //前置：强制转换为http协议的请求对象、响应对象 （转换原因：要使用子类中特有方法）
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        //请求登录路径放行
        String url = request.getRequestURL().toString();
        String permission = new String("");
        //判断是否需要权限true表示需要权限
        Boolean b = new Boolean(false);
        for (Map.Entry<String, String> entry : pUrl.entrySet()) {
            b = url.contains(entry.getValue());
            //判断是否需要权限需要则记录权限
            if (b) {
                permission = entry.getKey();
                break;
            }
        }
        //false 取反放行
        if (!b) {
            //不需要权限直接放行
            filterChain.doFilter(servletRequest, servletResponse);//放行
        } else {
            //获取用户名
            Object userSession = request.getSession().getAttribute("user");
            String user = "";
            if (Objects.nonNull(userSession)) {
                user = userSession.toString();
            }
            b = false;
            //获取取用户的权限
            List<String> userPermissionList = userRolePermissionDao.selectPermission(user);
            for (String userPermission : userPermissionList) {
                //判断是否用于权限
                b = userPermission.equals(permission);
                if (b) {
                    //有权限放行
                    filterChain.doFilter(servletRequest, servletResponse);//放行
                    break;
                }
            }

            if (!b) {
                //没有权限响应没有权限，修改访问路径
                servletRequest.getRequestDispatcher("/user/role/error").forward(servletRequest, servletResponse);
            }
        }
    }

}