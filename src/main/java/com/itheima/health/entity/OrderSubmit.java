package com.itheima.health.entity;

import com.itheima.health.dao.SetMealDao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PROJECT_NAME : heima-health
 * NAME : OrderSubmit
 * USER : sun
 * DATE : 2023/6/5
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderSubmit {
    private String SetMealId;
    private String sex;
    private String orderDate;
    private String name;
    private String telephone;
    private String validateCode;
    private String idCard;
}
