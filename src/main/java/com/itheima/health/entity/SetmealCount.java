package com.itheima.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PROJECT_NAME : heima-health
 * NAME : SetmealCountResult
 * USER : sun
 * DATE : 2023/6/4
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetmealCount {
    private String name;
    private Integer value;
    private Number proportion;
}
