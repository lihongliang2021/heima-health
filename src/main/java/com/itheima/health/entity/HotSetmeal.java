package com.itheima.health.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PROJECT_NAME : heima-health
 * NAME : HotSetmeal
 * USER : sun
 * DATE : 2023/6/4
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HotSetmeal {
    private String name;
    private Integer setmeal_count;
    private Number proportion;
}
