package com.itheima.health.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubmitOrderDTO {

    private Integer id;
    private String idCard;//身份证号
    private String name;//姓名
    private Date orderDate;//预约日期
    private Integer setmealId;//体检套餐id
    private String sex;//性别
    private String telephone;//手机号
    private String validateCode;//手机号验证码

    public Date getbirthDayByIdCard(String idCard){
        String year = idCard.substring(6, 10);//调用substring方法返回相关字段
        String month =idCard.substring(10, 12);
        String day = idCard.substring(12, 14);
        String birthday = year + "-" + month + "-" + day;
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");//定义一个时间转换格式“年-月-日”
        Date date = null;
        try {     //捕获类型转换（解析）异常
            date = fmt.parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
