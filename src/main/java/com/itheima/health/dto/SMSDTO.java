package com.itheima.health.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * PROJECT_NAME: heima-health-team
 * NAME: SMSDTO
 * USER: 10341
 * DATE: 2023/6/5
 * DESCRIPTION :
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSDTO {
    private String validateCode;
    private String telephone  ;
}
